#!/usr/bin/perl -Tw 

# mgh: mutiplexing-gdb-helper
#
# Copyright (C) 2014 Emmanuel Thomé
#
# This is a simple wrapper allowing the output of multiple programs to be
# recieved and used at a single terminal. The concept is inspired from
# pgdb, Copyright (C)  2009 Jim Keener (but this is a rewrite based on
# select(), and not on posix message queues).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# It is recommended to wrap this with rlwrap.
#
# There is no authentication whatsoever. (Note though that clients may
# not create command text).

use strict; 
use warnings;
use IO::Socket; 
use IO::Handle;
use Fcntl;
use Getopt::Std;
# use Net::hostent;
use Carp::Always;
use POSIX;

my %options=();
getopts("qp:",\%options);
my $quiet = defined $options{q};
# use port 7890 as default 
my $port = defined $options{p} ? $options{p} :  7890; 
$port =~ /^(\d+)$/ or die; $port=$1;

my $banner = <<EOF;
mgh: mutiplexing-gdb-helper

Copyright (C) 2014 Emmanuel Thomé
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Command window syntax: "Choose X", "Choose *", and "X <something>" are special. The rest is passed to the chosen clients, based on the ids they provided.

EOF

print $banner unless $quiet;

#create a socket, make it reusable 
my $proto = getprotobyname('tcp'); 
socket(SERVER, PF_INET, SOCK_STREAM, $proto) or die "socket: $!"; 
setsockopt(SERVER, SOL_SOCKET, SO_REUSEADDR, 1) or die "setsock: $!"; 
my $paddr = sockaddr_in($port, INADDR_ANY);
bind(SERVER, $paddr) or die "bind: $!"; 
listen(SERVER, SOMAXCONN) or die "listen: $!"; 

my $clients={};

$|=1;

my $unique_client_id = 0;

sub interpret_client_lines {
    my $k = shift @_;
    my $cq = $clients->{$k};
    my $id = $cq->{'id'};
    $id="unnamed-$k" unless defined $id;
    for (@_) {
        if (/^mgh-control:(\d+)$/) {
            $id = $1;
            print STDERR "Client $id: new ($cq->{'conn_info'})\n";
            $cq->{'id'}=$id;
            next;
        }
        print "$id $_";
    }
}

sub read_linebased {
    my ($fh, $queue) = @_;
    my $text;
    my $n = sysread($fh, $text, 1024);
    if (!defined($n) || $n == 0) {
        my @lines;
        push @lines, $$queue if $$queue;
        $$queue="";
        return @lines;
    }
    $text = $$queue . $text;
    my @lines = split(/^/, $text);
    if ($text !~ /\n$/) {
        $$queue = pop @lines;
    } else {
        $$queue = "";
    }
    return @lines;
}

sub interpret_selector {
    my $t = shift @_;
    my $s={};
    if ($t eq '*') {
        # for my $k (keys %$clients) {
        # $s->{$clients->{$k}->{'id'}} = 1;
        # }
        return $t;
    } else {
        for my $x (split(',',$t)) {
            $s->{$x}=1;
        }
    }
    return $s;
}

sub print_selector {
    my $s = shift @_;
    if (ref $s eq "") {
        return $s;
    } else {
        return join(", ", sort { $a <=> $b } keys(%$s));
    }
}

sub is_selected {
    my ($selector, $id) = @_;
    return 1 if ref $selector eq "";
    return exists $selector->{$id};
}


my $selector=interpret_selector('*');

my $caught_INT;
$SIG{'INT'} = sub {
    # print "Caught ^C!\n";
    $caught_INT = 1;
};

sub pass_INT {
    return unless $caught_INT;
    print "Passing ^C! to ", print_selector($selector), "\n";
    $caught_INT = undef;
    for my $k (keys %$clients) {
        my $cq = $clients->{$k};
        my $id = $cq->{'id'};
        $id="unnamed-$k" unless defined $id;
        next unless is_selected($selector, $id);
        my $fd = fileno($cq->{'socket'});
        syswrite($cq->{'socket'}, "mgh-control: ^C\n");
    }
}

sub interpret_stdin_lines {
    for (@_) {
        if (/^choose (\d+(?:,\d+)*|\*)$/) {
            $selector = interpret_selector($1);
            print "Current selector: ", print_selector($selector), "\n";
            next;
        }
        my $s = $selector;
        if (/^\?$/) {
            print "Current selector: ", print_selector($selector), "\n";
            next;
        }
        if (s/^(\d+)\s//) {
            $s = {$1=>1};
        }
        for my $k (keys %$clients) {
            my $cq = $clients->{$k};
            my $id = $cq->{'id'};
            $id="unnamed-$k" unless defined $id;
            next unless is_selected($s, $id);
            my $fd = fileno($cq->{'socket'});
            syswrite($cq->{'socket'}, $_);
        }
    }

}

my $stdin_queue="";
my $want_quit=0;

while(1) {
    my ($rout, $rin); $rin = "";
    vec($rin, fileno(SERVER), 1) = 1;
    vec($rin, fileno(STDIN), 1) = 1;

    my $n = scalar keys %$clients;
    ## print STDERR "We now have $n clients\n";

    for my $k (keys %$clients) {
        vec($rin, fileno($clients->{$k}->{'socket'}), 1) = 1;
    }

    $!=undef;
    my ($nfound, $timeleft) = select($rout = $rin, undef, undef, 10.0);

    if (!$nfound) {
        my @kill;
        for my $k (keys %$clients) {
            if (!defined $clients->{$k}->{'id'}) {
                if ($clients->{$k}->{'flag'}++) {
                    print STDERR "Client unnamed-$k: ($clients->{$k}->{'conn_info'}) still has no name, killing\n";
                    push @kill, $k;
                    next;
                }
            }
        }
        delete $clients->{$_} for @kill;
        next;
    }

    if ($! == EINTR) {
        pass_INT if $caught_INT;
        next;
    }

    die $! if $!;

    if (vec($rout, fileno(SERVER), 1)) {
        my $client;
        my $cpaddr = accept($client, SERVER);
        if (defined($cpaddr)) {
            # my $cname='you';
            my ($cport, $ciaddr) = sockaddr_in($cpaddr);
            my $cname = gethostbyaddr($ciaddr, AF_INET);

            my $ip = inet_ntoa($ciaddr);

            my $conn_info = "$cname [$ip], port $cport";

            my $now = scalar localtime;
            my $banner = "Hello there, $cname, it's now $now\n";
            syswrite($client, $banner);
            $clients->{$unique_client_id}={ socket=>$client, rqueue=>"", conn_info=>$conn_info };
            $unique_client_id++;
        } else {
            print STDERR "Connection on server socket, no cpaddr ?! ($nfound, $timeleft)\n";
        }
    }

    my @dead;
    for my $k (keys %$clients) {
        my $cq = $clients->{$k};
        my $client = $cq->{'socket'};
        next unless vec($rout, fileno($client), 1);
        my @lines = read_linebased($client, \$cq->{'rqueue'});
        if (!scalar @lines && !$cq->{'rqueue'}) {
            my $id = $cq->{'id'};
            $id="unnamed-$k" unless defined $id;
            print STDERR "Client $id: gone\n";
            push @dead, $k;
            next;
        }
        interpret_client_lines($k, @lines);
    }
    delete $clients->{$_} for @dead;

    if (vec($rout, fileno(STDIN), 1)) {
        my @lines = read_linebased(*STDIN, \$stdin_queue);
        if (!scalar @lines && !$stdin_queue) {
            last if $want_quit;
            print STDERR "Sure you want to quit ? (hit ^D another time to confirm)\n";
            $want_quit++;
            next;
        }
        $want_quit=0;
        interpret_stdin_lines(@lines);
    }
}

print STDERR "Killing remaining clients\n";
close $_->{'socket'} for values %$clients;
wait();
close SERVER; 
