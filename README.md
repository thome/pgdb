pgdb is a wrapper allowing the output of multiple programs to be recieved
and used at a single terminal

Original work: Copyright (C)  2009 Jim Keener ([link](https://github.com/jimktrains/pgdb))
Evolutions and rewrite: Copyright (C)  2013- Emmanuel Thomé

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

This is a set of perl scripts (client and server) that redirect the input and output of gdb (or any program really) over the network to a server, so that multiple nodes can run gdb without the need for each to have a terminal, as gdb usually needs.

HOW TO USE
----------

start a server in one window. rlwrap provides readline support, but is
not strictly necessary

```
rlwrap ./server3.pl [-p <port>]
```

run an mpi program from another window

```
mpiexec [[mpiexec args]] ./client3.pl gdb --args <executable> <args>
```

(for the tech-savvy: `server3.pl` does a select, `client3.pl` does a
simple exec)


When the debugging output comes to the server:
```
a <command> - sends a command to all nodes
# <command> - sends a command to node #
```

Other commands get passed to all ranks (but see below)

A special command is recognized:
```
 select <selector>
```

where the selector is either `*` or a comma-separated list of ranks. This
arranges so that the non-prefixed commands get passed to only these
ranks. `select *` reverts to the default behavior.

