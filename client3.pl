#! /usr/bin/perl -w 

# mgh: mutiplexing-gdb-helper
#
# Copyright (C) 2014 Emmanuel Thomé
#
# This is a simple wrapper allowing the output of multiple programs to be
# recieved and used at a single terminal. The concept is inspired from
# pgdb, Copyright (C)  2009 Jim Keener (but this is a rewrite based on
# select(), and not on posix message queues).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


use strict;
use warnings;
use Socket; 
use FileHandle;
use Fcntl;
use IPC::Open3;
use POSIX;

my $progid = $ENV{'OMPI_COMM_WORLD_RANK'};
# initialize host and port 
my $host = 'localhost'; 
my $port = 7890; 

while (scalar @ARGV) {
    if ($ARGV[0] eq '--') {
        shift @ARGV;
        last;
    }
    if ($ARGV[0] eq '--server' || $ARGV[0] eq '-s') {
        shift @ARGV;
        my $info = shift @ARGV;
        die unless defined($info);
        if ($info=~/^(.*):(\d+)/) {
            $port=$2;
            $host=$1;
        } else {
            $host=$info;
        }
        next;
    }
    if ($ARGV[0] eq '--port' || $ARGV[0] eq '-p') {
        shift @ARGV;
        die unless defined($port=shift @ARGV);
        next;
    }
    if ($ARGV[0] eq '--id') {
        shift @ARGV;
        die unless defined($progid=shift @ARGV);
        next;
    }
    last;
}

die "Must give program an ID" unless defined $progid;

my $proto = getprotobyname('tcp'); 

# get the port address 
my $iaddr = inet_aton($host); 
my $paddr = sockaddr_in($port, $iaddr); 
# create the socket, connect to the port 

$^F=999;

socket(SOCKET, PF_INET, SOCK_STREAM, $proto) or die "socket: $!"; 
connect(SOCKET, $paddr) or die "connect: $!"; 

my $banner;
sysread(SOCKET, $banner, 1024);
print "banner: $banner";

select(SOCKET); $|=1; print "mgh-control:$progid\n";

# close STDIN;
# close STDERR;
# close STDOUT;

# open STDIN, "<&SOCKET";
# open STDOUT, ">&SOCKET";
# open STDERR, ">&SOCKET";
# exec @ARGV;

my $pid = open3(\*CHLD_IN, \*CHLD_OUT, \*CHLD_ERR, @ARGV);


my ($upstream, $downstream, $downstream_cooked) = ("","","");

while(1) {
    my ($rout, $rin);
    my ($wout, $win);

    vec($rin, fileno(SOCKET), 1) = 1;
    vec($rin, fileno(CHLD_OUT), 1) = 1;
    vec($rin, fileno(CHLD_ERR), 1) = 1;
    vec($win, fileno(SOCKET), 1) = 1 if length $upstream;
    vec($win, fileno(CHLD_IN), 1) = 1 if length $downstream_cooked;

    $!=undef;
    my ($nfound, $timeleft) = select($rout = $rin, $wout = $win, undef, 10.0);

    next unless $nfound;

    if ($! == EINTR) {
        print "$!\n";
        next;
    }

    die $! if $!;

    if (length $upstream && vec($wout, fileno(SOCKET), 1)) {
        # print STDERR "up\n";
        my $n = syswrite(SOCKET, $upstream);
        $upstream = substr($upstream, $n);
    }

    if (length $downstream_cooked && vec($wout, fileno(CHLD_IN), 1)) {
        # print STDERR "down\n";
        my $n = syswrite(CHLD_IN, $downstream_cooked);
        $downstream_cooked = substr($downstream_cooked, $n);
    }

    if (vec($rout, fileno(SOCKET), 1)) {
        # print STDERR "new-data down\n";
        if (!defined($downstream)) { $downstream = ""; }
        my $x;
        my $n = sysread(SOCKET, $x, 1024);
        if ($n == 0) {
            print STDERR "$progid Server side died\n";
            close(CHLD_IN);
            close(CHLD_OUT);
            close(CHLD_ERR);
            close(STDIN);
            close(STDOUT);
            close(STDERR);
            close(SOCKET);
            # kill $pid;
            exit 0;
        }
        $downstream .= $x;
    }

    if (vec($rout, fileno(CHLD_OUT), 1)) {
        # print STDERR "new-data up\n";
        if (!defined($upstream)) { $upstream = ""; }
        my $x;
        my $n = sysread(CHLD_OUT, $x, 1024);
        if ($n == 0) {
            print STDERR "$progid Client side died\n";
            close(CHLD_IN);
            close(CHLD_OUT);
            close(CHLD_ERR);
            close(STDIN);
            close(STDOUT);
            close(STDERR);
            close(SOCKET);
            # kill $pid;
            exit 0;
        }
        $upstream .= $x;
    }
    if (vec($rout, fileno(CHLD_ERR), 1)) {
        if (!defined($upstream)) { $upstream = ""; }
        my $x;
        my $n = sysread(CHLD_ERR, $x, 1024);
        if ($n == 0) {
            print STDERR "$progid Client side died\n";
            last;
        }
        $upstream .= $x;
    }

    # Now filter-out the $downstream variable to check for control data.
    next unless $downstream;
    my @lines = split(/^/, $downstream);
    $downstream = "";
    for (@lines) {
        if (!/\n$/) {
            $downstream = $_;
            last;
        }
        if (/^mgh-control: \^C$/) {
            # print STDERR "$progid Passing ^C\n";
            kill 'INT', $pid;
            next;
        }
        # print STDERR "$progid Down: $_";
        $downstream_cooked .= $_;
    }
}


