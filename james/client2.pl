#! /usr/bin/perl -w 
#    pgdb is a wrapper allowing the output of multiple programs to be recieved
#      and used at a single terminal
#    Copyright (C)  2009 Jim Keener
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    You can contact the author via email at jkeener@psc.edu or snail mail at:
#	  James Keener
#	  Pittsburgh Supercomputing Center
#	  300 S. Craig St.
#	  Pittsburgh, Pa 15312


use strict;
use warnings;
use Socket; 
use IPC::Open3;
use FileHandle;

my $progid = $ENV{'OMPI_COMM_WORLD_RANK'};
# initialize host and port 
my $host = 'localhost'; 
my $port = 7890; 

while (scalar @ARGV) {
    if ($ARGV[0] eq '--') {
        shift @ARGV;
        last;
    }
    if ($ARGV[0] eq '--server' || $ARGV[0] eq '-s') {
        shift @ARGV;
        my $info = shift @ARGV;
        die unless defined($info);
        if ($info=~/^(.*):(\d+)/) {
            $port=$2;
            $host=$1;
        } else {
            $host=$info;
        }
        next;
    }
    if ($ARGV[0] eq '--port' || $ARGV[0] eq '-p') {
        shift @ARGV;
        die unless defined($port=shift @ARGV);
        next;
    }
    if ($ARGV[0] eq '--id') {
        shift @ARGV;
        die unless defined($progid=shift @ARGV);
        next;
    }
    last;
}

die "Must give program an ID" unless defined $progid;

my $proto = getprotobyname('tcp'); 

# get the port address 
my $iaddr = inet_aton($host); 
my $paddr = sockaddr_in($port, $iaddr); 
# create the socket, connect to the port 

socket(SOCKET, PF_INET, SOCK_STREAM, $proto) or die "socket: $!"; 
connect(SOCKET, $paddr) or die "connect: $!"; 
SOCKET->autoflush(1);

my $childpid = open3(\*CHILDSTDIN, \*CHILDSTDOUT, \*CHILDSTDERR, @ARGV);

#remote pid : remote mpi id : remote hostname
my $line = <SOCKET>;
# hostname returns a nl
print SOCKET "D:$childpid:$progid:".`hostname`;
#print "$progpid:$progid:" . `hostname` . "\n";
$line = <SOCKET>;


if(my $mypid = fork()){
    while (defined(my $cmd=<SOCKET>)) {
        print CHILDSTDIN $cmd; 
        last if $cmd eq 'quit';
    }
    print CHILDSTDIN "quit\n";
    print "bye\n";
} else {
    my ($oline, $eline, $line);
    # FIXME -- we need to select(), really.
    while(defined($oline = <CHILDSTDOUT>) or defined($eline = <CHILDSTDERR>)){
        $line = defined $oline ? $oline : "";
        $line = $line."E:$eline" if defined $eline;

        #print $line;
        print SOCKET $line;

        undef $oline;
        undef $eline;
    }
    print "rank $progid ended\n";
}

wait();
close SOCKET or die "close: $!"
